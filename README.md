Python Tools
---

[![pipeline status](https://gitlab.com/osechet/pythontools/badges/master/pipeline.svg)](https://gitlab.com/osechet/pythontools/commits/master)
[![coverage report](https://gitlab.com/osechet/pythontools/badges/master/coverage.svg)](https://gitlab.com/osechet/pythontools/commits/master)
[![codecov](https://codecov.io/gl/osechet/pythontools/branch/master/graph/badge.svg)](https://codecov.io/gl/osechet/pythontools)


Python Tools provides several utility functions and tools that can be used in Python projects.

This is mainly a dependency for my own packages and projects but they may be useful to others.

* autovenv provides function to automatically manage a virtual env (creation, update, ...)
* env provides environment related functions such as set_env that creates a context in which the environment can be changed.
* gitlab provides functions to communicate with a GitLab server through its REST API.
* pathutils provides path handling functions such as the chdir context function.
...

## Contributing

* Setup your enviroment:

```bash
python3 -m venv ./venv
. ./venv/bin/activate
python -m pip install --upgrade pip
python -m pip install -r ./requirements-dev.txt
```

* Run tests:

```bash
python -m pytest tests --cov=pythontools
```

## Publishing

```bash
python setup.py sdist
twine upload dist/*
```
