#!/usr/bin/env python3
"""
A example on how to use the pythontools' init module.
"""

import os

from scripts.pythontools import init

def main():
    """
    The main function.
    """
    project_path = os.path.dirname(os.path.realpath(__file__))
    init.run(project_path, 'venv')

if __name__ == '__main__':
    main()
