autopep8==1.4
codecov==2.0.15
pylint==2.1.1
pytest==3.8.2
pytest-cov==2.6.0
setuptools==40.4.3
twine==1.12.1
