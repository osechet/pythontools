"""
autovenv module tests.
"""

import os
import tempfile
from unittest import mock

import pytest

from pythontools import autovenv


class TestProject:
    """
    Test class for the Project class.
    """

    def test_has_venv_no_venv(self):
        """
        Test the has_venv method with no venv.
        """
        venv_dir = 'venv'
        with tempfile.TemporaryDirectory() as project_path:
            project = autovenv.Project(project_path, venv_dir)
            assert not project.has_venv()

    def test_has_venv_not_a_venv_dir(self):
        """
        Test the has_venv method with an invalid venv dir.
        """
        venv_dir = 'venv'
        with tempfile.TemporaryDirectory() as project_path:
            os.makedirs(os.path.join(project_path, venv_dir))
            project = autovenv.Project(project_path, venv_dir)
            assert not project.has_venv()

    def test_create_venv(self):
        """
        Test the create_venv method.
        """
        venv_dir = 'venv'
        with tempfile.TemporaryDirectory() as project_path:
            venv_path = os.path.join(project_path, venv_dir)
            project = autovenv.Project(project_path, venv_dir)
            assert not os.path.exists(venv_path)
            project.create_venv()
            assert os.path.exists(venv_path)
            assert project.has_venv()
            # A second call should not fail
            project.create_venv()

    @mock.patch('pythontools.autovenv.subprocess.Popen')
    def test_create_venv_mock(self, mock_popen):
        """
        Test the create_venv method with mocked subprocess.
        """
        mock_process = mock.MagicMock(**{
            '__enter__.return_value': mock.Mock(**{
                'communicate.return_value': ('output', 'error'),
                'poll.return_value': 0,
            }),
        })
        mock_popen.return_value = mock_process

        venv_dir = 'venv'
        with tempfile.TemporaryDirectory() as project_path:
            venv_path = os.path.join(project_path, venv_dir)
            project = autovenv.Project(project_path, venv_dir)
            assert not os.path.exists(venv_path)
            project.create_venv()
            mock_popen.assert_has_calls([
                mock.call([mock.ANY, '-Im', 'ensurepip', '--upgrade', '--default-pip'],
                          stderr=-2, stdout=-1),
            ], True)

    def test_is_venv_activated_no_venv(self):
        """
        Test the is_venv_activated method with no venv.
        """
        venv_dir = 'venv'
        with tempfile.TemporaryDirectory() as project_path:
            venv_path = os.path.join(project_path, venv_dir)
            project = autovenv.Project(project_path, venv_dir)
            assert not os.path.exists(venv_path)
            assert not project.is_venv_activated()

    def test_activate_venv(self):
        """
        Test the activate_venv method with no venv.
        """
        venv_dir = 'venv'
        with tempfile.TemporaryDirectory() as project_path:
            project = autovenv.Project(project_path, venv_dir)
            with mock.patch.object(project, 'has_venv', return_value=False) as mock_has_venv:
                with mock.patch.object(project, 'create_venv') as mock_create_venv:
                    with mock.patch('platform.system', return_value='Linux'):
                        with mock.patch('pythontools.env.source_env') as mock_source_env:
                            with mock.patch('pythontools.autovenv.subprocess.Popen') as mock_popen:
                                with mock.patch('pythontools.autovenv.exit') as mock_exit:
                                    project.activate_venv()
                                    mock_has_venv.assert_called_once()
                                    mock_create_venv.assert_called_once()
                                    mock_source_env.assert_called_once_with(os.path.join(project_path, venv_dir, 'bin', 'activate'))
                                    mock_popen.assert_called_once()
                                    mock_exit.assert_called_once()

                    mock_has_venv.reset_mock()
                    mock_create_venv.reset_mock()
                    with mock.patch('platform.system', return_value='Windows'):
                        with mock.patch('pythontools.env.source_env') as mock_source_env:
                            with mock.patch('pythontools.autovenv.subprocess.Popen') as mock_popen:
                                with mock.patch('pythontools.autovenv.exit') as mock_exit:
                                    project.activate_venv()
                                    mock_has_venv.assert_called_once()
                                    mock_create_venv.assert_called_once()
                                    mock_source_env.assert_called_once_with(os.path.join(project_path, venv_dir, 'Scripts', 'activate.bat'))
                                    mock_popen.assert_called_once()
                                    mock_exit.assert_called_once()

    def test_is_venv_activated_not_activated_venv(self):
        """
        Test the is_venv_activated method with a not activated venv.
        """
        venv_dir = 'venv'
        with tempfile.TemporaryDirectory() as project_path:
            venv_path = os.path.join(project_path, venv_dir)
            project = autovenv.Project(project_path, venv_dir)
            project.create_venv()
            assert os.path.exists(venv_path)
            assert not project.is_venv_activated()

    def test_setup_venv_no_requirements(self):
        """
        Test the setup_venv method with no requirements.
        """
        venv_dir = 'venv'
        with tempfile.TemporaryDirectory() as project_path:
            project = autovenv.Project(project_path, venv_dir)
            with mock.patch.object(project, 'ensure_venv') as mock_ensure_venv:
                with mock.patch('pythontools.autovenv.subprocess.check_output') as mock_check_output:
                    project.setup_venv()
                    mock_ensure_venv.assert_called_once()
                    mock_check_output.assert_called_once_with(['python', '-m', 'pip', 'install', '--upgrade', 'pip'])

    def test_setup_venv_with_requirements(self):
        """
        Test the setup_venv method.
        """
        venv_dir = 'venv'
        with tempfile.TemporaryDirectory() as project_path:
            project = autovenv.Project(project_path, venv_dir)
            with mock.patch.object(project, 'ensure_venv') as mock_ensure_venv:
                with mock.patch('pythontools.autovenv.subprocess.check_output') as mock_check_output:
                    with mock.patch('os.path.exists', return_value=False) as mock_exists:
                        project.setup_venv(['foo.txt', 'bar.txt'])
                        mock_ensure_venv.assert_called_once()
                        mock_check_output.assert_called_once_with(['python', '-m', 'pip', 'install', '--upgrade', 'pip'])
                        mock_exists.assert_has_calls([
                            mock.call(os.path.join(project_path, 'foo.txt')),
                            mock.call(os.path.join(project_path, 'bar.txt')),
                        ])

                    mock_ensure_venv.reset_mock()
                    mock_check_output.reset_mock()
                    with mock.patch('os.path.exists', return_value=True) as mock_exists:
                        project.setup_venv(['foo.txt', 'bar.txt'])
                        mock_ensure_venv.assert_called_once()
                        mock_check_output.assert_has_calls([
                            mock.call(['python', '-m', 'pip', 'install', '--upgrade', 'pip']),
                            mock.call(['python', '-m', 'pip', 'install', '-r', os.path.join(project_path, 'foo.txt')]),
                            mock.call(['python', '-m', 'pip', 'install', '-r', os.path.join(project_path, 'bar.txt')]),
                        ])
                        mock_exists.assert_has_calls([
                            mock.call(os.path.join(project_path, 'foo.txt')),
                            mock.call(os.path.join(project_path, 'bar.txt')),
                        ])

    def test_ensure_venv(self):
        """
        Test the ensure_venv method.
        """
        venv_dir = 'venv'
        with tempfile.TemporaryDirectory() as project_path:
            project = autovenv.Project(project_path, venv_dir)
            with mock.patch.object(project, 'has_venv', return_value=False) as mock_has_venv:
                with mock.patch.object(project, 'create_venv') as mock_create_venv:
                    with mock.patch.object(project, 'is_venv_activated', return_value=False) as mock_is_venv_activated:
                        with mock.patch.object(project, 'activate_venv') as mock_activate_venv:
                            project.ensure_venv()
                            mock_has_venv.assert_called_once()
                            mock_create_venv.assert_called_once()
                            mock_is_venv_activated.assert_called_once()
                            mock_activate_venv.assert_called_once()
            with mock.patch.object(project, 'has_venv', return_value=True) as mock_has_venv:
                with mock.patch.object(project, 'create_venv') as mock_create_venv:
                    with mock.patch.object(project, 'is_venv_activated', return_value=True) as mock_is_venv_activated:
                        with mock.patch.object(project, 'activate_venv') as mock_activate_venv:
                            project.ensure_venv()
                            mock_has_venv.assert_called_once()
                            mock_create_venv.assert_not_called()
                            mock_is_venv_activated.assert_called_once()
                            mock_activate_venv.assert_not_called()

def test_ensure_venv():
    """
    Test the ensure_venv function.
    """
    venv_dir = 'venv'
    with tempfile.TemporaryDirectory() as project_path:
        with mock.patch.object(autovenv.Project, 'ensure_venv') as mock_ensure_venv:
            autovenv.ensure_venv(project_path, venv_dir)
            mock_ensure_venv.assert_called_once()

def test_setup_venv():
    """
    Test the setup_venv function.
    """
    venv_dir = 'venv'
    with tempfile.TemporaryDirectory() as project_path:
        with mock.patch.object(autovenv.Project, 'setup_venv') as mock_setup_venv:
            autovenv.setup_venv(project_path, venv_dir, [])
            mock_setup_venv.assert_called_once_with([])

            mock_setup_venv.reset_mock()
            autovenv.setup_venv(project_path, venv_dir, requirements=['foo.txt', 'bar.txt'])
            mock_setup_venv.assert_called_once_with(['foo.txt', 'bar.txt'])
