"""
env module tests.
"""

import os
import platform
from unittest import mock

import pytest

from pythontools import env


@pytest.fixture()
def testdata_dir():
    """
    Returns the path to the testdata directory.
    """
    module_path = os.path.dirname(os.path.realpath(__file__))
    return os.path.join(module_path, 'testdata')


class TestSourceEnv:
    """
    Test class for the source_env function.
    """

    def test_no_file(self):
        """
        Test sourcing a non-existant file.
        """
        with pytest.raises(FileNotFoundError):
            env.source_env('not_a_file')


    def test_bash_on_windows(self, testdata_dir):
        """
        Test sourcing a bash env file on Windows.
        """
        if platform.system() == 'Windows':
            env_file = os.path.join(testdata_dir, 'env_file')
            env.source_env(env_file)


    def test_batch_on_unix(self, testdata_dir):
        """
        Test sourcing a batch env file on Unix.
        """
        if platform.system() != 'Windows':
            env_file = os.path.join(testdata_dir, 'env_file.bat')
            env.source_env(env_file)


    def test_valid(self, testdata_dir):
        """
        Test sourcing a valid env file.
        """
        if platform.system() == 'Windows':
            env_file = os.path.join(testdata_dir, 'env_file.bat')
        else:
            env_file = os.path.join(testdata_dir, 'env_file')
        environ = env.source_env(env_file)
        assert environ['EXPORTED_VAR1'] == 'exported value 1'
        assert environ['EXPORTED_VAR2'] == 'exported value 2'
        assert environ['EXPORTED_VAR3'] == 'exported value 2'
        assert environ['MULTILINE_EXPORTED_VAR'] == 'line 1\nline 2'

    def test_with_mock(self):
        """
        Test the method using mock.
        """
        env_file = '/path/to/env_file'
        output = b'VAR1=VALUE1\0VAR2=VALUE2'
        with mock.patch('os.path.exists', return_value=True) as mock_exists:
            with mock.patch('platform.system', return_value='Linux') as mock_system:
                with mock.patch('subprocess.check_output', return_value=output) as mock_check_output:
                    environ = env.source_env(env_file)
                    assert environ == {'VAR1': 'VALUE1', 'VAR2': 'VALUE2'}
                    mock_exists.assert_called_once()
                    mock_system.assert_has_calls([
                        mock.call(),
                        mock.call(),
                    ])
                    mock_check_output.assert_called_once_with([
                        'bash', '-c', 'source /path/to/env_file && env --null'
                    ])
            mock_exists.reset_mock()
            with mock.patch('platform.system', return_value='Darwin') as mock_system:
                with mock.patch('subprocess.check_output', return_value=output) as mock_check_output:
                    with mock.patch('sys.executable', **{'__str__.return_value': 'python'}):
                        environ = env.source_env(env_file)
                        assert environ == {'VAR1': 'VALUE1', 'VAR2': 'VALUE2'}
                        mock_exists.assert_called_once()
                        mock_system.assert_called_once()
                        mock_check_output.assert_called_once_with([
                            'bash', '-c',
                            'source /path/to/env_file && python -c \'import os; [print("%s=%s" % (key, value), end="\\0") for key, value in os.environ.items()]\''
                        ])
            mock_exists.reset_mock()
            with mock.patch('platform.system', return_value='Windows') as mock_system:
                with mock.patch('subprocess.check_output', return_value=output) as mock_check_output:
                    with mock.patch('os.remove') as mock_remove:
                        environ = env.source_env(env_file)
                        assert environ == {'VAR1': 'VALUE1', 'VAR2': 'VALUE2'}
                        mock_exists.assert_called_once()
                        mock_system.assert_has_calls([
                            mock.call(),
                            mock.call(),
                        ])
                        mock_check_output.assert_called_once_with(['cmd', '/C', mock.ANY])
                        mock_remove.assert_called_once()

class TestSetEnv:
    """
    Test class for the set_env function.
    """

    def test_empty_dict(self):
        """
        Test if the environment remains the same using an empty dict.
        """
        old_env = dict(os.environ)
        with env.set_env({}):
            assert os.environ == old_env
        assert os.environ == old_env

    def test_valid(self):
        """
        Test if the environment is modified inside the context and is properly restored when
        exiting the context.
        """
        old_env = dict(os.environ)
        with env.set_env({'VAR1': 'value 1'}):
            assert os.environ['VAR1'] == 'value 1'
        assert os.environ == old_env
