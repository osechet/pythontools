"""
env module tests.
"""

import os
import tempfile
from io import StringIO
from unittest import mock

from pythontools import gitlab


class TestGitLab:
    """
    Test class for the GitLab class.
    """

    @mock.patch('pythontools.gitlab.GitLab._request')
    def test_find_project_not_found(self, mock_request):
        """
        Test find_project method when no project matches.
        """
        mock_request.return_value = []
        server = gitlab.GitLab('https://gitlab.com', 'token')
        project = server.find_project('user', 'project_name')
        mock_request.assert_called_with('https://gitlab.com/api/v4/users/user/projects?search=project_name')
        assert not project

    @mock.patch('pythontools.gitlab.GitLab._request')
    def test_find_project_found(self, mock_request):
        """
        Test find_project method.
        """
        mock_request.return_value = [{'name': 'project_name'}]
        server = gitlab.GitLab('https://gitlab.com', 'token')
        project = server.find_project('user', 'project_name')
        mock_request.assert_called_with('https://gitlab.com/api/v4/users/user/projects?search=project_name')
        assert project
        assert project['name'] == 'project_name'

    @mock.patch('pythontools.gitlab.GitLab._request')
    def test_find_project_partial_name(self, mock_request):
        """
        Test find_project method when a project have been found but does not have the exact name.
        """
        mock_request.return_value = [{'name': 'project_name_other'}]
        server = gitlab.GitLab('https://gitlab.com', 'token')
        project = server.find_project('user', 'project_name')
        mock_request.assert_called_with('https://gitlab.com/api/v4/users/user/projects?search=project_name')
        assert not project

    @mock.patch('pythontools.gitlab.GitLab._request')
    def test_find_project_several_matches(self, mock_request):
        """
        Test find_project method when several matches have been found.
        """
        mock_request.return_value = [{'name': 'project_name_other'}, {'name': 'project_name'}]
        server = gitlab.GitLab('https://gitlab.com', 'token')
        project = server.find_project('user', 'project_name')
        mock_request.assert_called_with('https://gitlab.com/api/v4/users/user/projects?search=project_name')
        assert project
        assert project['name'] == 'project_name'

    @mock.patch('pythontools.gitlab.GitLab._request')
    def test_find_project_several_matches_not_found(self, mock_request):
        """
        Test find_project method when several matches have been found but none is the specified
        project.
        """
        mock_request.return_value = [{'name': 'project_name_other'}, {'name': 'project_name_another'}]
        server = gitlab.GitLab('https://gitlab.com', 'token')
        project = server.find_project('user', 'project_name')
        mock_request.assert_called_with('https://gitlab.com/api/v4/users/user/projects?search=project_name')
        assert not project

    @mock.patch('pythontools.gitlab.GitLab._find_project')
    def test_find_project_from_group(self, mock_find_project):
        """
        Test find_project_from_group method.
        """
        mock_find_project.return_value = None
        server = gitlab.GitLab('https://gitlab.com', 'token')
        server.find_project_from_group('group', 'project_name')
        mock_find_project.assert_called_with('https://gitlab.com/api/v4/groups/group/projects?search=project_name',
                                             'project_name')

    @mock.patch('pythontools.gitlab.GitLab._request')
    def test_list_pipelines(self, mock_request):
        """
        Test list_pipelines method.
        """
        mock_request.return_value = None
        server = gitlab.GitLab('https://gitlab.com', 'token')
        server.list_pipelines({'id': 123})
        mock_request.assert_called_with('https://gitlab.com/api/v4/projects/123/pipelines')

    @mock.patch('pythontools.gitlab.GitLab._request')
    def test_list_jobs(self, mock_request):
        """
        Test list_jobs method.
        """
        mock_request.return_value = None
        server = gitlab.GitLab('https://gitlab.com', 'token')
        server.list_jobs({'id': 123}, 456)
        mock_request.assert_called_with('https://gitlab.com/api/v4/projects/123/pipelines/456/jobs')

    @mock.patch('pythontools.gitlab.GitLab._download')
    def test_download_artifacts(self, mock_download):
        """
        Test the download_artifacts method.
        """
        server = gitlab.GitLab('https://gitlab.com', 'token')
        project = {'id': 123}
        job = {
            'id': 456,
            'name': 'job_name',
            'artifacts_file': {'filename': 'artifacts.zip'},
        }
        with tempfile.TemporaryDirectory() as download_dir:
            with mock.patch("builtins.open", mock.mock_open(read_data="data")) as mock_open:
                server.download_artifacts(project, job, download_dir)
                mock_open.assert_called_with(os.path.join(download_dir, 'artifacts.zip'), 'wb')
                mock_download.assert_called_with('https://gitlab.com/api/v4/projects/123/jobs/456/artifacts',
                                                 mock_open.return_value)

    @mock.patch('urllib.request.build_opener')
    def test_request(self, mock_build_opener):
        """
        Test the _request method.
        """
        attrs = {'open.return_value': StringIO('[{"data": "value"}]')}
        mock_opener = mock.Mock(**attrs)
        mock_build_opener.return_value = mock_opener

        server = gitlab.GitLab('https://gitlab.com', 'token')
        url = 'https://gitlab.com'
        response = server._request(url)
        assert mock_opener.addheaders == [('PRIVATE-TOKEN', 'token')]
        mock_opener.open.assert_called_with(url)
        assert response == [{'data': 'value'}]

    def _download_tester(self, mock_build_opener, monitor):
        """
        Test the _download method.
        """
        data1 = bytes('remote data', 'utf-8')
        data2 = bytes('remote data', 'utf-8')
        connection_attrs = {
            '__enter__.return_value': mock.Mock(**{
                'info.return_value': {'Content-Length': len(data1) + len(data2)},
                'read.side_effect': [data1, data2, None],
            }),
        }
        mock_connection = mock.MagicMock(**connection_attrs)
        opener_attrs = {'open.return_value': mock_connection}
        mock_opener = mock.Mock(**opener_attrs)
        mock_build_opener.return_value = mock_opener

        server = gitlab.GitLab('https://gitlab.com', 'token')
        url = 'https://server.local/file'
        with tempfile.NamedTemporaryFile() as dest_file:
            if monitor:
                server._download(url, dest_file, monitor)
            else:
                server._download(url, dest_file)
            assert mock_opener.addheaders == [('PRIVATE-TOKEN', 'token')]
            mock_opener.open.assert_called_with(url)
            dest_file.seek(0)
            assert dest_file.read() == data1 + data2

    @mock.patch('urllib.request.build_opener')
    def test_download(self, mock_build_opener):
        """
        Test the _download method.
        """
        self._download_tester(mock_build_opener, None)

    @mock.patch('urllib.request.build_opener')
    def test_download_with_monitor(self, mock_build_opener):
        """
        Test the _download method.
        """
        mock_monitor = mock.Mock(return_value=None)
        self._download_tester(mock_build_opener, mock_monitor)
        mock_monitor.assert_has_calls([mock.call('file', 11, 22), mock.call('file', 22, 22)])
