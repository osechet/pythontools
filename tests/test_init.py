"""
env module tests.
"""

import os
from unittest import mock

from pythontools import init

class TestInitGitSubmodules:
    """
    Test class for the init_git_submodules function.
    """

    def test_no_submodule(self):
        """
        Test when there is no submodules.
        """
        project_path = '/path/to/project'
        with mock.patch('os.path.exists', return_value=False) as mock_exists:
            with mock.patch('subprocess.check_output') as mock_check_output:
                init.init_git_submodules(project_path)
                mock_exists.assert_called_once()
                mock_check_output.assert_not_called()

    def test_with_submodule(self):
        """
        Test when there is a gitsubmodule file.
        """
        project_path = '/path/to/project'
        with mock.patch('os.path.exists', return_value=True) as mock_exists:
            with mock.patch('subprocess.check_output') as mock_check_output:
                with mock.patch('pythontools.pathutils.chdir'):
                    init.init_git_submodules(project_path)
                    mock_exists.assert_called_once_with(os.path.join(project_path, '.gitmodules'))
                    mock_check_output.assert_called_with(['git', 'submodule', 'update', '--init'])


def test_activate_command():
    """
    Test the activate_command function.
    """
    with mock.patch('platform.system', return_value='Linux'):
        result = init.activate_command('venv_dir')
        assert result is not None
        assert isinstance(result, str)

    with mock.patch('platform.system', return_value='Windows'):
        result = init.activate_command('venv_dir')
        assert result is not None
        assert isinstance(result, str)


def test_run():
    """
    Test the run function.
    """
    project_path = '/path/to/project'
    venv_name = 'venv'
    with mock.patch('argparse.ArgumentParser') as mock_argparse:
        with mock.patch('pythontools.autovenv.ensure_venv') as mock_ensure_venv:
            with mock.patch('pythontools.init.init_git_submodules') as mock_init_git_submodules:
                with mock.patch('pythontools.autovenv.setup_venv') as mock_setup_venv:
                    init.run(project_path, venv_name)
                    mock_argparse.assert_called_once()
                    mock_ensure_venv.assert_called_once_with(project_path, venv_name)
                    mock_init_git_submodules.assert_called_once_with(project_path)
                    mock_setup_venv.assert_called_once_with(project_path, venv_name,
                                                            requirements=['requirements.txt',
                                                                            'requirements-dev.txt'])
