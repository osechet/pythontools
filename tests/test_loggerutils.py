"""
loggerutils module tests.
"""

import logging
import os
from unittest import mock

from pythontools import loggerutils


class TestColoredFormatter:
    """
    Test class for the ColoredFormatter class.
    """

    @mock.patch('pythontools.loggerutils.datetime')
    def test_format(self, mock_datetime):
        """
        Test sourcing a non-existant file.
        """
        mock_datetime.fromtimestamp.return_value = mock_datetime
        mock_datetime.strftime.return_value = '01:02:03'
        formatter = loggerutils.ColoredFormatter()
        record = logging.LogRecord('name', logging.INFO, 'pathname', 123, 'msg', None, None)
        assert formatter.format(record) == '01:02:03 - msg'

        record_with_args = logging.LogRecord('name', logging.INFO, 'pathname', 123, 'msg %s %d',
                                             ('a', 1), None)
        assert formatter.format(record_with_args) == '01:02:03 - msg a 1'
