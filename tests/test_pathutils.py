"""
pathutils module tests.
"""

import os

import pytest

from pythontools import pathutils


@pytest.fixture()
def testdata_dir():
    """
    Returns the path to the testdata directory.
    """
    module_path = os.path.dirname(os.path.realpath(__file__))
    return os.path.join(module_path, 'testdata')


class TestChDir:
    """
    Test class for the chdir function.
    """

    def test_invalid_dir(self):
        """
        Test the function with an invalid directory.
        """
        cwd = os.getcwd()
        with pytest.raises(FileNotFoundError):
            with pathutils.chdir('not-a-directory'):
                # pytest.fail('Call should raise a FileNotFoundError')
                pass
        assert os.getcwd() == cwd


    def test_same_dir(self):
        """
        Test the function with the current directory.
        """
        cwd = os.getcwd()
        with pathutils.chdir(cwd):
            assert os.getcwd() == cwd


    def test_dir_change(self, testdata_dir):
        """
        Test if the directory changes in the context and is restored when exiting.
        """
        cwd = os.getcwd()
        with pathutils.chdir(testdata_dir):
            assert os.getcwd() == testdata_dir
        assert os.getcwd() == cwd
